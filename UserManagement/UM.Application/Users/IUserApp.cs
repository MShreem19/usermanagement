﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UM.CoreEntity.Base;
using UM.CoreEntity.Users;
using UM.CoreEntity.Users.DTO;

namespace UM.Application.Users
{
    public interface IUserApp
    {
        Task<UMResponse> Create(CreateUserDto user);
        Task<UMResponse> Update(UpdateUserDto user);
        Task<UMResponse<User>> Get(int id);
        Task<UMResponse<List<User>>> List(SearchUserDto user);
    }
}
