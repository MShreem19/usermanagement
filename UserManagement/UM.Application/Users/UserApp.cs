﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UM.CoreEntity.Base;
using UM.CoreEntity.Users;
using UM.CoreEntity.Users.DTO;
using static UM.CoreEntity.Base.Enums;

namespace UM.Application.Users
{
    public class UserApp : IUserApp
    {

        public IUnitOfWork unitOfWork;
        public UserApp(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<UMResponse> Create(CreateUserDto user)
        {
            try
            {
                
                using (unitOfWork)
                {
                    var isValidDto = await CheckUser(user, unitOfWork);
                    if (isValidDto != StatusCode.Success)
                        return new UMResponse() { StatusCode = isValidDto };


                    await unitOfWork.Users.Create(new User()
                    {
                        LoginName = user.LoginName,
                        DisplayName = user.DisplayName,
                        DateOfBirth = user.DateOfBirth.Date,
                        CountryId = user.CountryId,
                        Address = user.Address,
                        Status = UserStatus.Pinding,

                        CreateDate = DateTime.Now,
                        LastUpdateDate = DateTime.Now,
                    });
                    await unitOfWork.Complet();
                    return new UMResponse();
                }
            }
            catch (Exception)
            {
                return new UMResponse()
                {
                    StatusCode = StatusCode.GeneralError
                };
            }
        }

        public async Task<UMResponse<User>> Get(int id)
        {
            try
            {
                using (unitOfWork)
                {
                    var user  = await unitOfWork.Users.Get(id);
                    if (user == null)
                        return new UMResponse<User>() { StatusCode = StatusCode.NotFound };

                    return new UMResponse<User>() { Data = user, };
                }
            }
            catch (Exception)
            {
                return new UMResponse<User>() { StatusCode = StatusCode.GeneralError };
            }
        }

        public async Task<UMResponse<List<User>>> List(SearchUserDto model)
        {
            try
            {
                using (unitOfWork)
                {
                    var result = await unitOfWork.Users.Find(where: (x =>
                                                            (string.IsNullOrEmpty(model.Name) || x.DisplayName.Contains(model.Name)) &&
                                                            (string.IsNullOrEmpty(model.Address) || x.Address.Contains(model.Address)) &&
                                                            (!model.FromDateOfBirth.HasValue || x.DateOfBirth.Date >= model.FromDateOfBirth.Value.Date) &&
                                                            (!model.ToDateOfBirth.HasValue || x.DateOfBirth.Date <= model.ToDateOfBirth.Value.Date) &&
                                                            (!model.Status.HasValue || x.Status == model.Status.Value) &&
                                                            (!model.Country.HasValue || x.CountryId == model.Country.Value)
                                                            ));

                    if (result == null || !result.Any())
                        return new UMResponse<List<User>>() { StatusCode = StatusCode.NotFound };

                    return new UMResponse<List<User>>
                    {
                        Data = result.ToList(),
                    };
                }
            }
            catch (Exception ex)
            {
                return new UMResponse<List<User>>()
                {
                    StatusCode = StatusCode.GeneralError,
                };
            }
        }

        public async Task<UMResponse> Update(UpdateUserDto model)
        {
            try
            {
                using (unitOfWork)
                {
                    var user = await unitOfWork.Users.Get(model.Id);
                    if (user == null)
                        return new UMResponse() { StatusCode = Enums.StatusCode.NotFound };

                    user.LoginName = model.LoginName ?? user.LoginName;
                    user.DisplayName = model.DisplayName ?? user.DisplayName;
                    user.Address = model.Address ?? user.Address;
                    user.DateOfBirth = model.DateOfBirth ?? user.DateOfBirth;
                    user.CountryId = model.CountryId ?? user.CountryId;
                    user.Status = model.Status ?? user.Status;
                    user.Salary = model.Salary ?? user.Salary;


                    await unitOfWork.Complet();
                    return new UMResponse();

                }
            }
            catch (Exception ex)
            {
                return new UMResponse()
                {
                    StatusCode = StatusCode.GeneralError
                };
            }
        }


        private async Task<StatusCode> CheckUser(CreateUserDto user, IUnitOfWork unitOfWork)
        {
            var result = await unitOfWork.Users.Find(x => (x.LoginName.Equals(user.LoginName)));
            if (result != null)
                return StatusCode.DataAlreadyExists;

            return StatusCode.Success;
        }
    }
}
