﻿using System;
using System.Threading.Tasks;
using UM.CoreEntity.Base;
using UM.CoreEntity.Users.DTO;

namespace UM.Application.Account
{
    public interface IAccountApp
    {
        Task<UMResponse<Guid>> Login(LoginDto model);
        Task<UMResponse> SetPassword(SetPasswordDto model);

        Task<UMResponse> ValidateToken(Guid token);
    }
}
