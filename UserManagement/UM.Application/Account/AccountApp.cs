﻿using System;
using System.Threading.Tasks;
using UM.CoreEntity.Base;
using UM.CoreEntity.Users.DTO;

namespace UM.Application.Account
{
    public class AccountApp : IAccountApp
    {

        public IUnitOfWork unitOfWork;
        public AccountApp(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<UMResponse<Guid>> Login(LoginDto model)
        {
            try
            {
                using (unitOfWork)
                {

                    var result = await unitOfWork.Users.SingleOrDefaultAsync(where: (x => x.LoginName.Equals(model.LoginName) && x.Password.Equals(model.Password)));
                    if (result == null)
                    {
                        return new UMResponse<Guid>()
                        {
                            StatusCode = Enums.StatusCode.InvalidUsernameOrPassword
                        };
                    }

                    var token = Guid.NewGuid();
                    result.Token = token;


                    return new UMResponse<Guid>()
                    {
                        Data = token
                    };
                }
            }
            catch (Exception)
            {
                return new UMResponse<Guid>() { StatusCode = Enums.StatusCode.GeneralError };
            }
        }

        public async Task<UMResponse> SetPassword(SetPasswordDto model)
        {
            throw new NotImplementedException();
        }

        public async Task<UMResponse> ValidateToken(Guid token)
        {

            try
            {
                using (unitOfWork)
                {

                    var result = await unitOfWork.Users.SingleOrDefaultAsync(where: (x => x.Token == token));
                    if (result == null)
                    {
                        return new UMResponse<Guid>()
                        {
                            StatusCode = Enums.StatusCode.InvalidToken
                        };
                    }

                    return new UMResponse<Guid>();
                }
            }
            catch (Exception)
            {
                return new UMResponse<Guid>() { StatusCode = Enums.StatusCode.GeneralError };
            }
        }
    }
}
