﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UM.EntityFramework;

namespace UM.Application.Repositories
{
    public class Repository<Entity> : IRepository<Entity> where Entity : class
    {
        private readonly UmContext Context;
        public Repository(UmContext context)
        {
            Context = context;
        }

        public async Task Create(Entity entity)
        {
            await Context.Set<Entity>().AddAsync(entity);
        }

        public async Task<IEnumerable<Entity>> Find<Include>(Expression<Func<Entity, bool>> where, Expression<Func<Entity, Include>> include)
        {
            return await Context.Set<Entity>().Include(include).Where(where).ToListAsync();
        }

        public async Task<IEnumerable<Entity>> Find(Expression<Func<Entity, bool>> where)
        {
            return await Context.Set<Entity>().Where(where).ToListAsync();
        }

        public async Task<Entity> Get(int id)
        {
            return await Context.Set<Entity>().FindAsync(id);
        }

        public async Task<IEnumerable<Entity>> GetAll()
        {
            return await Context.Set<Entity>().ToListAsync();
        }

        public async Task<Entity> SingleOrDefaultAsync(Expression<Func<Entity, bool>> expression)
        {
            var result = await Context.Set<Entity>().SingleOrDefaultAsync(expression);
            return result;
        }
    }
}
