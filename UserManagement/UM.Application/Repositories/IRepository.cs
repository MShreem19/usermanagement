﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace UM.Application.Repositories
{
    public interface IRepository<Entity> where Entity : class
    {
        Task<Entity> Get(int request);
        Task<IEnumerable<Entity>> GetAll();
        Task<IEnumerable<Entity>> Find<Include>(Expression<Func<Entity, bool>> where, Expression<Func<Entity, Include>> include);
        Task<IEnumerable<Entity>> Find(Expression<Func<Entity, bool>> where);
        Task<Entity> SingleOrDefaultAsync(Expression<Func<Entity, bool>> where);
        Task Create(Entity entity);
    }
}
