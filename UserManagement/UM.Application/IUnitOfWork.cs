﻿using System;
using System.Threading.Tasks;
using UM.Application.Repositories;
using UM.CoreEntity.Lookups;
using UM.CoreEntity.Users;

namespace UM.Application
{
    public interface IUnitOfWork : IDisposable
    {
        Task<int> Complet();

        IRepository<User> Users { get; }
        IRepository<Country> Countries { get; }
    }
}
