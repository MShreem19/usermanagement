﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UM.CoreEntity.Base;
using UM.CoreEntity.Lookups;
using UM.CoreEntity.Lookups.DTO;
using static UM.CoreEntity.Base.Enums;

namespace UM.Application.Countries
{
    public class CountryApp : ICountryApp
    {

        public IUnitOfWork unitOfWork;
        public CountryApp(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<UMResponse> Create(CreateLookupDto lookup)
        {
            try
            {

                var isValidDto = await CheckLookup(lookup, unitOfWork);
                if (isValidDto != StatusCode.Success)
                    return new UMResponse() { StatusCode = isValidDto };


                using (unitOfWork)
                {
                    await unitOfWork.Countries.Create(new Country()
                    {
                        NameAr = lookup.NameAr,
                        NameEn = lookup.NameEn,

                        CreateDate = DateTime.Now,
                        LastUpdateDate = DateTime.Now
                    });

                    await unitOfWork.Complet();
                    return new UMResponse();
                }
            }
            catch (System.Exception)
            {
                return new UMResponse()
                {
                    StatusCode = Enums.StatusCode.GeneralError,
                };
            }
        }

        public async Task<UMResponse<Country>> Get(int id)
        {
            try
            {
                using (unitOfWork)
                {
                    var user = await unitOfWork.Countries.Get(id);
                    if (user == null)
                        return new UMResponse<Country>() { StatusCode = StatusCode.NotFound };

                    return new UMResponse<Country>() { Data = user, };
                }
            }
            catch (Exception)
            {
                return new UMResponse<Country>() { StatusCode = StatusCode.GeneralError };
            }
        }

        public async Task<UMResponse<List<Country>>> List(ListLookupDto model)
        {
            try
            {
                using (unitOfWork)
                {
                    var result = await unitOfWork.Countries.Find(where: (x =>
                                                            (string.IsNullOrEmpty(model.NameAr) || x.NameAr.Contains(model.NameAr)) &&
                                                            (string.IsNullOrEmpty(model.NameEn) || x.NameEn.Contains(model.NameEn))
                                                            ));

                    if (result == null || !result.Any())
                        return new UMResponse<List<Country>>() { StatusCode = StatusCode.NotFound };


                    return new UMResponse<List<Country>>
                    {
                        Data = result.ToList(),
                    };
                }
            }
            catch (Exception ex)
            {
                return new UMResponse<List<Country>>()
                {
                    StatusCode = StatusCode.GeneralError,
                };
            }
        }

        public async Task<UMResponse> Update(UpdateLookupDto lookup)
        {
            try
            {
                using (unitOfWork)
                {
                    var isValidDto = await CheckLookup(lookup, unitOfWork);
                    if (isValidDto != StatusCode.Success)
                        return new UMResponse() { StatusCode = isValidDto };


                    var result = await unitOfWork.Countries.Get(lookup.Id);

                    if (result == null)
                        return new UMResponse() { StatusCode = Enums.StatusCode.NotFound };


                    result.NameAr = lookup.NameAr;
                    result.NameEn = lookup.NameEn;
                    result.LastUpdateDate = DateTime.Now;

                    await unitOfWork.Complet();
                    return new UMResponse();
                }
            }
            catch (System.Exception)
            {
                return new UMResponse()
                {
                    StatusCode = Enums.StatusCode.GeneralError,
                };
            }
        }


        private async Task<StatusCode> CheckLookup(CreateLookupDto lookup, IUnitOfWork unitOfWork)
        {

            var result = await unitOfWork.Countries.Find(x => (x.NameAr.Equals(lookup.NameAr)) || (x.NameEn.Equals(lookup.NameEn)));

            if (result != null)
                return StatusCode.DataAlreadyExists;

            return StatusCode.Success;
        }


        private async Task<StatusCode> CheckLookup(UpdateLookupDto lookup, IUnitOfWork unitOfWork)
        {

            var result = await unitOfWork.Countries.Find(x => x.Id != lookup.Id && ( x.NameAr.Equals(lookup.NameAr) || x.NameEn.Equals(lookup.NameEn)) );

            if (result != null)
                return StatusCode.DataAlreadyExists;

            return StatusCode.Success;
        }
    }
}
