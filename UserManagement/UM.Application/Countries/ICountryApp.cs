﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UM.CoreEntity.Base;
using UM.CoreEntity.Lookups;
using UM.CoreEntity.Lookups.DTO;

namespace UM.Application.Countries
{
    public interface ICountryApp
    {
        Task<UMResponse> Create(CreateLookupDto lookup);
        Task<UMResponse> Update(UpdateLookupDto lookup);
        Task<UMResponse<Country>> Get(int id);
        Task<UMResponse<List<Country>>> List(ListLookupDto model);

    }
}
