﻿using System.Threading.Tasks;
using UM.Application.Repositories;
using UM.CoreEntity.Lookups;
using UM.CoreEntity.Users;
using UM.EntityFramework;

namespace UM.Application
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly UmContext Context;

        public UnitOfWork(UmContext Context)
        {
            Context = new SQLContext(new Microsoft.EntityFrameworkCore.DbContextOptions<SQLContext>());
            Users = new Repository<User>(Context);
            Countries = new Repository<Country>(Context);
        }
        public async Task<int> Complet()
        {
            return await Context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Context.Dispose();
        }

        public IRepository<User> Users { get; private set; }

        public IRepository<Country> Countries { get; private set; }
    }
}
