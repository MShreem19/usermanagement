﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;
using UM.Application;
using UM.Application.Countries;
using UM.EntityFramework;
using UM.Framework.Configuration;

namespace UM.API
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IContainer ApplicationContainer { get; private set; }

        public IConfigurationRoot Configuration { get; private set; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddDbContext<SQLContext>(options =>
                       options.UseSqlServer(Configuration["ConnectionStrings:SQLConnection"]));

            services.AddSwaggerGen(sDoc =>
               sDoc.SwaggerDoc("v1", new Info { Title = "User Managment API", Version = "v1" })
            );

            //services.AddDbContext<MongoDBContext>(options =>
            //           options.UseMongoDb(Configuration["ConnectionStrings:SQLConnection"]));


            var builder = new ContainerBuilder();

            builder.RegisterType<ConfigurationAccessor>().As<IConfigurationAccessor>();
            builder.RegisterType<Log4NetLogger>().As<ILogger>();
            builder.RegisterType<CountryApp>().As<ICountryApp>();

            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();

            builder.Populate(services);
            ApplicationContainer = builder.Build();

            return new AutofacServiceProvider(ApplicationContainer);
        }


        public void Configure(IApplicationBuilder app, IApplicationLifetime appLifetime)
        {
            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });


            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            appLifetime.ApplicationStopped.Register(() => ApplicationContainer.Dispose());
        }
    }
}
