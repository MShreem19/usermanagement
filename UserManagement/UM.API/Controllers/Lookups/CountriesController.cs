﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UM.Application.Countries;
using UM.CoreEntity.Base;
using UM.CoreEntity.Lookups;
using UM.CoreEntity.Lookups.DTO;

namespace UM.API.Controllers.Lookups
{
    [ApiController]
    public class CountriesController : ControllerBase
    {

        private ICountryApp countryApp;
        private readonly UM.Framework.Logging.ILogger logger;


        public CountriesController(ICountryApp countryApp, UM.Framework.Logging.ILogger logger)
        {
            this.countryApp = countryApp;
            this.logger = logger;
        }


        [HttpPost("api/Countries/List")]
        public async Task<UMResponse<List<Country>>> List(ListLookupDto request)
        {
            var result = await countryApp.List(request);
            return result;
        }

        [HttpPost("api/Countries/Get")]
        public async Task<UMResponse<Country>> Get(int request)
        {
            var result = await countryApp.Get(request);
            return result;
        }

        [HttpPost("api/Countries/Create")]
        public async Task<UMResponse> Create(CreateLookupDto request)
        {
            if (!request.IsValid)
            {
                return new UMResponse()
                {
                    StatusCode = Enums.StatusCode.InvalidModel,
                };
            }

            var result = await countryApp.Create(request);
            return result;
        }

        [HttpPost("api/Countries/Update")]
        public async Task<UMResponse> Update(UpdateLookupDto request)
        {
            if (!request.IsValid)
            {
                return new UMResponse()
                {
                    StatusCode = Enums.StatusCode.InvalidModel,
                };
            }

            var result = await countryApp.Update(request);
            return result;
        }


        [HttpPost("api/Countries/Delete")]
        public void Delete(int id)
        {
        }
    }
}