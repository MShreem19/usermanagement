﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UM.Application.Account;
using UM.CoreEntity.Base;

namespace UM.API.Controllers
{
    [AllowAnonymous]
    [ApiController]
    public class AccountController : ControllerBase
    {
        IAccountApp acountApp;
        public AccountController(IAccountApp acountApp)
        {
            this.acountApp = acountApp;
        }


        [AllowAnonymous]
        [HttpGet("api/Account/Token")]
        public async Task<UMResponse<Guid>> GetToken(string loginName, string password)
        {
            if (string.IsNullOrWhiteSpace(loginName) || string.IsNullOrWhiteSpace(password))
                return new UMResponse<Guid>()
                {
                    StatusCode = Enums.StatusCode.InvalidModel,
                };


            var result = await acountApp.Login(new CoreEntity.Users.DTO.LoginDto() { LoginName = loginName, Password = password });
            
            //TODO: Stor Info To Application

            return result;
        }
    }
}