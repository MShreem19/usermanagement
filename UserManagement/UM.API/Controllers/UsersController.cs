﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UM.Application.Users;
using UM.CoreEntity.Base;
using UM.CoreEntity.Users;
using UM.CoreEntity.Users.DTO;

namespace UM.API.Controllers
{
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUserApp userApp;
        private readonly UM.Framework.Logging.ILogger logger;


        public UsersController(IUserApp userApp, UM.Framework.Logging.ILogger logger)
        {
            this.userApp = userApp;
            this.logger = logger;
        }


        [HttpPost("api/Users/List")]
        public async Task<UMResponse<List<User>>> List(SearchUserDto request)
        {
            var result = await userApp.List(request);
            return result;
        }

        [HttpPost("api/Users/Get")]
        public async Task<UMResponse<User>> Get(int request)
        {
            var result = await userApp.Get(request);
            return result;
        }

        [HttpPost("api/Users/Create")]
        public async Task<UMResponse> Create(CreateUserDto request)
        {
            if (!request.IsValid)
            {
                return new UMResponse()
                {
                    StatusCode = Enums.StatusCode.InvalidModel,
                };
            }

            var result = await userApp.Create(request);
            return result;
        }


        [HttpPost("api/Users/Update")]
        public async Task<UMResponse> Update(UpdateUserDto request)
        {
            if (!request.IsValid)
            {
                return new UMResponse()
                {
                    StatusCode = Enums.StatusCode.InvalidModel,
                };
            }

            var result = await userApp.Update(request);
            return result;
        }

        [HttpPost("api/Users/Delete")]
        public void Delete(int id)
        {
        }
    }
}
