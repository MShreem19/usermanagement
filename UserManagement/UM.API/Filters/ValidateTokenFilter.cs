﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Microsoft.AspNetCore.Authorization;

namespace UM.API.Filters
{
    public class ValidateTokenFilter : AuthorizationFilterAttribute //: IAuthenticationFilter
    {
        public override async Task OnAuthorizationAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            try
            {
                //if (actionContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true) || actionContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true))
                //    return;

                var tokenHeader = actionContext.Request.Headers.FirstOrDefault(h => h.Key == "Token");

                Guid token;
                if (string.IsNullOrEmpty(tokenHeader.Key)
                    || (!System.Guid.TryParse(tokenHeader.Value.FirstOrDefault().ToString(), out token)))
                {
                    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                    return; 
                }
                // ValidateToken 

            }
            catch (System.Exception)
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
    }
}
