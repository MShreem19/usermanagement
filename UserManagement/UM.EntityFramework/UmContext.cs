﻿using Microsoft.EntityFrameworkCore;
using UM.CoreEntity.Users;

namespace UM.EntityFramework
{
    public abstract class UmContext : DbContext
    {
        public UmContext(DbContextOptions contextOptions)
                    : base(contextOptions)
        {

        }
        DbSet<User> Users { get; set; }
    }

}
