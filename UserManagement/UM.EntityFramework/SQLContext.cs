﻿using Microsoft.EntityFrameworkCore;
using UM.CoreEntity.Users;

namespace UM.EntityFramework
{
    public class SQLContext : UmContext
    {
        public SQLContext(DbContextOptions<SQLContext> contextOptions) : base(contextOptions)
        {
        }

        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
    
}
