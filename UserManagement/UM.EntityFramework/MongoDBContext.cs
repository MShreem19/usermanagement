﻿using Microsoft.EntityFrameworkCore;
using UM.CoreEntity.Users;

namespace UM.EntityFramework
{
    public class MongoDBContext : UmContext
    {
        public MongoDBContext(DbContextOptions contextOptions) : base(contextOptions)
        {
        }

        public DbSet<User> Users { get; set; }
    }
}
