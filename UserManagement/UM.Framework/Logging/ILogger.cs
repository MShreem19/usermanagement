﻿using System;

namespace UM.Framework.Logging
{
    public interface ILogger
    {
        void Fatal(string message);

        void Error(string message, Exception exception);

        void Error(string message);

        void Warn(string message);

        void Info(string message);

        void Debug(string message);
    }
}
