﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using Microsoft.Extensions.DependencyModel;

namespace UM.Framework.Configuration
{
    public interface IConfigurationAccessor
    {
        NameValueCollection Configurations { get; }
        T Get<T>(string key, T defaultValue);
    }
}
