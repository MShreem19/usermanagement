﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;

namespace UM.Framework.Configuration
{

    public class ConfigurationAccessor : IConfigurationAccessor
    {
        /// <summary>
        /// Instance of Configurations Manager
        /// </summary>
        public NameValueCollection Configurations => ConfigurationManager.AppSettings;


        /// <summary>
        /// find configuration by the key and return its value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"> key of configuration</param>
        /// <param name="defaultValue"> this value will be returned if the key not exists </param>
        /// <returns></returns>
        public T Get<T>(string key, T defaultValue)
        {
            try
            {
                if (Configurations.AllKeys.Contains(key))
                {
                    return (T)Convert.ChangeType(Configurations[key], typeof(T));
                }
                return defaultValue;
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }
    }
}
