﻿using Autofac;
using UM.EntityFramework;
using UM.Framework.Configuration;

namespace UM.Framework.DataSource
{
    /// <summary>
    /// Check which Database Source enabled and registre it to DbContext
    /// </summary>
    public class DbContextModule : Autofac.Module
    {
        public IConfigurationAccessor configurationAccessor;
        protected override void Load(ContainerBuilder builder)
        {
            if (configurationAccessor.Get("CONTEXT_TYPE", "SQLServer").Equals("SQLServer"))
            {
                builder.RegisterType<SQLContext>().As<UmContext>().InstancePerDependency();
            }

            else if (configurationAccessor.Get("CONTEXT_TYPE", "MongoDB").Equals("MongoDB"))
            {
                builder.RegisterType<MongoDBContext>().As<UmContext>().InstancePerDependency();
            }

            builder.RegisterType<SQLContext>().As<UmContext>().InstancePerDependency();
        }
    }
}
