﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using UM.CoreEntity.Base;
using UM.CoreEntity.Lookups;
using static UM.CoreEntity.Base.Enums;

namespace UM.CoreEntity.Users
{
    public class User : BaseEntity
    {
        public string LoginName { get; set; }
        public string Password { get; set; }
        public Guid? Token { get; set; }
        public string DisplayName { get; set; }
        public DateTime DateOfBirth { get; set; }
        [ForeignKey(nameof(Country))]
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public string Address { get; set; }
        public UserStatus Status { get; set; }
        public decimal Salary { get; set; }
        public string ProfilePic { get; set; }
    }
}