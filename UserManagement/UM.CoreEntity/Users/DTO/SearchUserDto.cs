﻿using System;
using static UM.CoreEntity.Base.Enums;

namespace UM.CoreEntity.Users.DTO
{
    public class SearchUserDto
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public DateTime? FromDateOfBirth { get; set; }
        public DateTime? ToDateOfBirth { get; set; }
        public int? Country { get; set; }
        public UserStatus? Status { get; set; }
    }
}
