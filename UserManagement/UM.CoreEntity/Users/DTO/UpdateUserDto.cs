﻿using System;
using static UM.CoreEntity.Base.Enums;

namespace UM.CoreEntity.Users.DTO
{
    public class UpdateUserDto
    {
        public int Id { get; set; }
        public string LoginName { get; set; }
        public string DisplayName { get; set; }
        public string Address { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? CountryId { get; set; }
        public UserStatus? Status { get; set; }
        public decimal? Salary { get; set; }
        public bool IsValid
        {
            get
            {
                return CheckUserUpdateDTo();
            }
        }


        private bool CheckUserUpdateDTo()
        {
            if (this.Id < 1)
                return false;

            //TODO Check all Properties
            return true;
        }
    }
}
