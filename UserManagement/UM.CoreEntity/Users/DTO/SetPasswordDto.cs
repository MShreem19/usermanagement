﻿namespace UM.CoreEntity.Users.DTO
{
    public class SetPasswordDto
    {
        public string LogingName { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
