﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UM.CoreEntity.Users.DTO
{
    public class CreateUserDto
    {
        [Required]
        public string LoginName { get; set; }

        [Required]
        public string DisplayName { get; set; }

        [Required]
        public string Address { get; set; }


        //[Range(typeof(DateTime),minimum: "1800-1-1"]//, maximum: DateTime.Now.ToShortDateString())]
        public DateTime DateOfBirth { get; set; }

        [Range(typeof(int), "0", "999999")]
        public int CountryId { get; set; }

        public bool IsValid {
            get
            {
                return CheckUserCreateDTo();
            }
        }

        private bool CheckUserCreateDTo()
        {
            if (string.IsNullOrEmpty(this.LoginName))
                return false;

            if(string.IsNullOrEmpty(this.DisplayName))
                return false;

            if (string.IsNullOrEmpty(this.Address))
                return false;

            if (this.DateOfBirth.Date > DateTime.Now.Date || this.DateOfBirth.Date < DateTime.Parse("1800-1-1").Date)
                return false;

            if (this.CountryId < 0 || this.CountryId > 9999099) //TODO Check Country Repository
                return false;


            return true;
        }
    }
}
