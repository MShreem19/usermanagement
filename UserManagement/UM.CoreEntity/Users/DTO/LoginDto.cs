﻿namespace UM.CoreEntity.Users.DTO
{
    public class LoginDto
    {
        public string LoginName { get; set; }
        public string Password { get; set; }
    }
}
