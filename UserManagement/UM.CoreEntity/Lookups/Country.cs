﻿using UM.CoreEntity.Base;

namespace UM.CoreEntity.Lookups
{
    public class Country : BaseEntity, ILookup
    {
        public string NameAr { get; set; }
        public string NameEn { get; set; }
    }
}
