﻿namespace UM.CoreEntity.Lookups
{
    public interface ILookup
    {
        int Id { get; set; }
        string NameAr { get; set; }
        string NameEn { get; set; }
    }
}
