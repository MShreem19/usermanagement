﻿namespace UM.CoreEntity.Lookups.DTO
{
    public class CreateLookupDto
    {
        public string NameAr { get; set; }
        public string NameEn { get; set; }

        public bool IsValid {
            get {
                return CheckCreateLookupDto();
            }
        }


        private bool CheckCreateLookupDto()
        {
            if (string.IsNullOrWhiteSpace(this.NameAr))
                return false;

            if (string.IsNullOrWhiteSpace(this.NameEn))
                return false;

            return true;
        }
    }
}
