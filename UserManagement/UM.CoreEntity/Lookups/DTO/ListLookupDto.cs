﻿namespace UM.CoreEntity.Lookups.DTO
{

    public class ListLookupDto
    {
        public string NameAr { get; set; }
        public string NameEn { get; set; }
    }
}
