﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UM.CoreEntity.Lookups.DTO
{
    public class UpdateLookupDto
    {
        public int Id { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }

        public bool IsValid
        {
            get
            {
                return CheckCreateLookupDto();
            }
        }


        private bool CheckCreateLookupDto()
        {
            if (string.IsNullOrWhiteSpace(this.NameAr))
                return false;

            if (string.IsNullOrWhiteSpace(this.NameEn))
                return false;

            return true;
        }
    }
}
