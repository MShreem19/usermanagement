﻿using static UM.CoreEntity.Base.Enums;

namespace UM.CoreEntity.Base
{
    public class UMResponse
    {
        public StatusCode StatusCode { get; set; }
    }

    public class UMResponse<T> : UMResponse
    {
        public T Data { get; set; }
        public UMResponse()
        {
        }
        public UMResponse(T data)
        {
            Data = data;
        }
    }
}
