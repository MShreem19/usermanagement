﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UM.CoreEntity.Base
{
    public class UMRequest
    {
        public int CurrentUserId { get; set; }
        public Guid Token { get; set; }
    }

    public class UMRequest<T> : UMRequest
    {
        public T Data { get; set; }
        public UMRequest(T data)
        {
            Data = data;
        }
    }
}
