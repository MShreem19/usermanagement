﻿namespace UM.CoreEntity.Base
{
    public static class Enums
    {
        public enum StatusCode : int
        {
            Success = 0,
            GeneralError = 100,
            NotFound = 404,


            DataAlreadyExists = 1,
            InvalidModel = 2,
            InvalidUsernameOrPassword = 3,
            InvalidToken = 4

        }

        public enum UserStatus : int
        {
            Active = 1,
            Stopped = 2,
            Pinding = 3
        }
    }
}
